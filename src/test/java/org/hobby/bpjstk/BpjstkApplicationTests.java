package org.hobby.bpjstk;

import lombok.extern.slf4j.Slf4j;
import org.hobby.bpjstk.client.BpjsTkClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@SpringBootTest
class BpjstkApplicationTests {

	@Autowired
	private BpjsTkClient bpjsTkClient;


	@Test
	void getGeneralInfo_Test() {
		String username = "muhammad.abdulloh93@gmail.com";
		String password = "Pass@word1";
		var result = bpjsTkClient.getGeneralInfo(username, password);
		log.info(result.toString());
		assertNotNull(result);
	}

	@Test
	void getEmployment_Test() {
		String username = "muhammad.abdulloh93@gmail.com";
		String password = "Pass@word1";
		var result = bpjsTkClient.getEmploymentInfo(username, password);
		for (var item : result) {
			log.info(item.toString());
			assertNotNull(item);
		}
	}

	@Test
	void getSegment_Test() {
		String username = "muhammad.abdulloh93@gmail.com";
		String password = "Pass@word1";
		var result = bpjsTkClient.getSegmentInfo(username, password);
		for (var item : result) {
			log.info(item.toString());
			assertNotNull(item);
		}
	}

	@Test
	void getSalary_Test() {
		String username = "muhammad.abdulloh93@gmail.com";
		String password = "Pass@word1";
		var result = bpjsTkClient.getSalaryInfo(username, password);
		for (var item : result) {
			log.info(item.toString());
			assertNotNull(item);
		}
	}
}
