package org.hobby.bpjstk;

import lombok.extern.slf4j.Slf4j;
import org.hobby.bpjstk.helper.EncryptionHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest
class EncryptionTest {
    @Autowired
    private EncryptionHelper encryptionHelper;

    @Test
    void encrypt_test() {
        String email = "muhammad.abdulloh93@gmail.com";
        String password = "Pass@word1";
        assertNotNull(encryptionHelper.encrypt(email));
        assertNotNull(encryptionHelper.encrypt(password));
    }

    @Test
    void decrypt_test() {
        String emailCipher = "0EO2bWmkU1Pjxx9odsLvcnwiohLSbyFSSx3/DOj/7YW5QZFUosRTtg==";
        String passwordCipher = "I1YBV2+Z4oPSxVUkYZm66jEGjILK875E";
        assertEquals("muhammad.abdulloh93@gmail.com", encryptionHelper.decrypt(emailCipher));
        assertEquals("Pass@word1", encryptionHelper.decrypt(passwordCipher));
    }
}
