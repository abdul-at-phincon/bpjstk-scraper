package org.hobby.bpjstk.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CredentialDto {
    private String email;
    private String password;
}
