package org.hobby.bpjstk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.text.WordUtils;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BpjsTkEmployment {
    private String namaPerusahaan;
    private String statusKeps;
    private String tglKepsJp;
    private String tglPensiunJp;
    private String tglPniItrf;
    private int masaIurJp;
    private double nomUpahItrf;

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = WordUtils.capitalizeFully(namaPerusahaan);
    }

    public void setStatusKeps(String statusKeps) {
        this.statusKeps = statusKeps.equalsIgnoreCase("y") ? "Aktif" : "Tidak Aktif";
    }
}
