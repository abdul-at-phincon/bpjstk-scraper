package org.hobby.bpjstk.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.commons.text.WordUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class BpjsTkSegment {
    private String kodeTk;
    private String namaPerusahaan;
    private double nomSaldo;
    private String kodeSegmen;
    private String kpj;
    private String kodePerusahaan;
    private String npp;

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = WordUtils.capitalizeFully(namaPerusahaan);
    }
}
