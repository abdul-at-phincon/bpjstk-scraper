package org.hobby.bpjstk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BpjsTkLogin {
    private String token;
    private BpjsTkGeneral data;
    private String msg;
    private int ret;
}
