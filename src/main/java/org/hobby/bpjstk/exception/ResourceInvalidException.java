package org.hobby.bpjstk.exception;

public class ResourceInvalidException extends RuntimeException {
    public ResourceInvalidException() {
    }

    public ResourceInvalidException(Throwable cause) {
        super(cause);
    }

    public ResourceInvalidException(String message) {
        super(message);
    }

    public ResourceInvalidException(String message, Throwable cause) {
        super(message, cause);
    }
}
