package org.hobby.bpjstk.client;

import org.hobby.bpjstk.model.BpjsTkSalary;
import org.hobby.bpjstk.model.BpjsTkEmployment;
import org.hobby.bpjstk.model.BpjsTkGeneral;
import org.hobby.bpjstk.model.BpjsTkSegment;

import java.util.List;

public interface BpjsTkClient {
    List<BpjsTkSalary> getSalaryInfo(String email, String password);
    List<BpjsTkEmployment> getEmploymentInfo(String email, String password);
    BpjsTkGeneral getGeneralInfo(String email, String password);
    List<BpjsTkSegment> getSegmentInfo(String email, String password);
}
