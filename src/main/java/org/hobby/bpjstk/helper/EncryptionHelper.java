package org.hobby.bpjstk.helper;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.stereotype.Service;

@Service
public class EncryptionHelper {
    private static final String SEED = "youarethereason";
    private final StandardPBEStringEncryptor encryptor;

    public EncryptionHelper() {
        encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(SEED);
    }

    public String encrypt(String plainText) {
        return encryptor.encrypt(plainText);
    }

    public String decrypt(String cipher) {
        return encryptor.decrypt(cipher);
    }
}
